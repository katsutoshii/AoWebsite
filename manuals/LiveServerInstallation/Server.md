# Installing the Live Server Extension

I will keep this brief since you can always look up a tutorial on YouTube on installing new VSCode extensions.

The purpose of this extension is to give you a look at the changes you are making in real time. If you are confident in making the changes, then you can always skip that step in the guides, but I recommend it so that you know you aren't making any mistakes.

## 1. Open VSCode and press the extensions tab on the left

![file-name](GoToExtensions.png)

## 2. Type in "Live Server" into the search bar

![file-name](search.png)

## 3. Press the install button on the FIRST OPTION THAT POPS UP

![file-name](PressTheFirst.png)

### Done, the extension should be installed to your program :)
