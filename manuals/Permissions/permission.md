# Giving Members-Only Permissions to New Mems

Whenever we get new mems, they must be added to the list of users that have access to the members only page of the website. Otherwise they will not be able to access the calendar, the manuals or ban list.

The file you need to change is located is `manuals/.htcaccess` Whenever you have a a filename that starts with a period, that is called a hidden file. If you are unable to view the hidden files, you can simply search it on google, or ask the previous webmasters. On mac, you can press `CMD + SHIFT+ .` and that should enable hidden files.

## 1. Open the file /manuals/.htcaccess

You simply need to navigate to the manuals folder and press the .htcacces file

![file-name](HiddenFile.png)

## 2. Copy and Paste New Mem
The file should be a collection of names and netid's. To add a new member, simply copy the previous entry, paste at the end of the file and update the information to match the new member.

![file-name](CopyPaste.png)

In this example, I have copy and pasted the previous entry and highlighted the sections that need to be changed.

### NOTE: DO NOT MESS UP THE ORDER. Always just append at the end, not the middle.

[Uploading manual](/../UploadingToWebsite/Upload.md)

Now just upload to github, upload the files to the website, then done :)
